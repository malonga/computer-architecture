; BEGIN:helper

breakpoint:
ret

test_3_1:
addi sp, sp, -4
stw ra, 0 (sp)

addi s0, zero, N_GSA_COLUMNS
addi s1, zero, N_GSA_LINES
add a0,zero,zero
add a1,zero,zero
set_pixel_loop:
call set_pixel
addi a0, a0, 1
blt a0, s0, set_pixel_loop
addi a1, a1, 1
add a0, zero, zero
blt a1, s1, set_pixel_loop 



call clear_leds
addi a0, zero, 5
addi a1, zero, 0
call set_pixel
addi a0, zero, 6
call set_pixel
addi a1, zero, 1
addi a0, zero, 4
call set_pixel
addi a0, zero, 7
call set_pixel
addi a1, zero, 2
addi a0, zero, 3
call set_pixel
addi a0, zero, 8
call set_pixel
addi a1, zero, 3
call set_pixel
addi a0, zero, 3
call set_pixel
addi a1, zero, 4
call set_pixel
addi a0, zero, 8
call set_pixel
addi a1, zero, 5
call set_pixel
addi a0, zero, 4
call set_pixel
addi a0, zero, 7
call set_pixel
addi a0, zero, 3
call set_pixel
addi a1, zero, 6
addi a0, zero, 4
call set_pixel
addi a0, zero, 5
call set_pixel
addi a0, zero, 6
call set_pixel
addi a0, zero, 7
call set_pixel
addi a1, zero, 7
addi a0, zero, 5
call set_pixel
addi a0, zero, 6
call set_pixel

; "O"

call clear_leds
addi a0, zero, 3
addi a1, zero, 1
call set_pixel
addi a0, zero, 7
call set_pixel
addi a0, zero, 3
addi a1, zero, 2
call set_pixel
addi a0, zero, 4
call set_pixel
addi a0, zero, 5
call set_pixel
addi a0, zero, 6
call set_pixel
addi a0, zero, 7
call set_pixel
addi a0, zero, 3
addi a1, zero, 3
call set_pixel
addi a0, zero, 4
call set_pixel
addi a0, zero, 5
call set_pixel
addi a0, zero, 3
addi a1, zero, 4
call set_pixel
addi a0, zero, 4
call set_pixel
addi a0, zero, 3
addi a1, zero, 5
call set_pixel
addi a0, zero, 4
call set_pixel
addi a0, zero, 5
call set_pixel
addi a0, zero, 3
addi a1, zero, 6
call set_pixel
addi a0, zero, 5
call set_pixel
addi a0, zero, 6
call set_pixel
addi a0, zero, 3
addi a1, zero, 7
call set_pixel
addi a0, zero, 6
call set_pixel
addi a0, zero, 7
call set_pixel

; "K"

ldw ra, 0 (sp)
addi sp, sp, 4

ret

test_3_2_and_3_3:

addi sp, sp, -4
stw ra, 0 (sp)

add s0, zero, zero
addi s1 ,zero, 0b111111111111
addi s2, zero, 0b010101010101
addi s3, zero, 0b001100110011
addi s4, zero, 0b000111000111
addi s5, zero, 0b000011110000
addi s6, zero, 0b000001111100
addi s7, zero, 0b000000111111

br test_set_gsa

addi t0, zero, 0

stw s0, GSA0 (t0)
stw s7, GSA1 (t0)
addi t0, t0, 4
stw s1, GSA0 (t0)
stw s6, GSA1 (t0)
addi t0, t0, 4
stw s2, GSA0 (t0)
stw s5, GSA1 (t0)
addi t0, t0, 4
stw s3, GSA0 (t0)
stw s4, GSA1 (t0)
addi t0, t0, 4
stw s4, GSA0 (t0)
stw s3, GSA1 (t0)
addi t0, t0, 4
stw s5, GSA0 (t0)
stw s2, GSA1 (t0)
addi t0, t0, 4
stw s6, GSA0 (t0)
stw s1, GSA1 (t0)
addi t0, t0, 4
stw s7, GSA0 (t0)
stw s0, GSA1 (t0)
addi t0, t0, 4

;GSA0 is 000000000000(s0)|111111111111(s1)|010101010101|001100110011|000111000111|000011110000
;GSA1 is 000000111111(s7)|000001111100(s6)|000011110000|000111000111|001100110011

test_get_gsa:

add t0, zero, zero
stw t0, GSA_ID (zero)
addi a0, zero, 0
call get_gsa
call breakpoint
addi a0, zero, 1
call get_gsa
call breakpoint
addi a0, zero, 2
call get_gsa
call breakpoint
addi a0, zero, 3
call get_gsa
call breakpoint
addi a0, zero, 4
call get_gsa
call breakpoint
addi a0, zero, 5
call get_gsa
call breakpoint
addi a0, zero, 6
call get_gsa
call breakpoint
addi a0, zero, 7
call get_gsa
call breakpoint

addi t0, zero, 1
stw t0, GSA_ID (zero)

addi a0, zero, 0
call get_gsa
call breakpoint
addi a0, zero, 1
call get_gsa
call breakpoint
addi a0, zero, 2
call get_gsa
call breakpoint
addi a0, zero, 3
call get_gsa
call breakpoint
addi a0, zero, 4
call get_gsa
call breakpoint
addi a0, zero, 5
call get_gsa
call breakpoint
addi a0, zero, 6
call get_gsa
call breakpoint
addi a0, zero, 7
call get_gsa
call breakpoint

br test_draw_gsa


test_set_gsa: ; This assumes draw_gsa works
add a0, s0, zero
addi a1, zero, 0
call set_gsa
call draw_gsa
call breakpoint
add a0, s1, zero
addi a1, zero, 1
call set_gsa
call draw_gsa
call breakpoint
add a0, s2, zero
addi a1, zero, 2
call set_gsa
call draw_gsa
call breakpoint
add a0, s3, zero
addi a1, zero, 3
call set_gsa
call draw_gsa
call breakpoint
add a0, s4, zero
addi a1, zero, 4
call set_gsa
call draw_gsa
call breakpoint
add a0, s5, zero
addi a1, zero, 5
call set_gsa
call draw_gsa
call breakpoint
add a0, s6, zero
addi a1, zero, 6
call set_gsa
call draw_gsa
call breakpoint
add a0, s7, zero
addi a1, zero, 7
call set_gsa
call draw_gsa
call breakpoint

ret

test_draw_gsa:
call draw_gsa
call breakpoint




ldw ra, 0 (sp)
addi sp, sp, 4

ret


test_3_4:

addi sp, sp, -4
stw ra, 0 (sp)

call random_gsa
call draw_gsa
call breakpoint

ldw ra, 0 (sp)
addi sp, sp, 4

ret

test_cell_fate:
addi sp, sp, -4
ldw ra, 0 (sp)


; Expected 0
addi a0, zero, 0
addi a1, zero, 0
call cell_fate
call breakpoint
addi a0, zero, 1
addi a1, zero, 0
call cell_fate
call breakpoint
addi a0, zero, 2
addi a1, zero, 0
call cell_fate
call breakpoint
addi a0, zero, 4
addi a1, zero, 0
call cell_fate
call breakpoint
addi a0, zero, 5
addi a1, zero, 0
call cell_fate
call breakpoint
addi a0, zero, 6
addi a1, zero, 0
call cell_fate
call breakpoint
addi a0, zero, 7
addi a1, zero, 0
call cell_fate
call breakpoint
addi a0, zero, 8
addi a1, zero, 0
call cell_fate
call breakpoint

addi a0, zero, 0
addi a1, zero, 1
call cell_fate
call breakpoint
addi a0, zero, 1
addi a1, zero, 1
call cell_fate
call breakpoint
addi a0, zero, 0
addi a1, zero, 4
call cell_fate
call breakpoint
addi a0, zero, 0
addi a1, zero, 5
call cell_fate
call breakpoint
addi a0, zero, 0
addi a1, zero, 6
call cell_fate
call breakpoint
addi a0, zero, 0
addi a1, zero, 7
call cell_fate
call breakpoint
addi a0, zero, 0
addi a1, zero, 8
call cell_fate
call breakpoint


; Expected 1

addi a0, zero, 3
addi a1, zero, 0
call cell_fate
call breakpoint

addi a0, zero, 2
addi a1, zero, 1
call cell_fate
call breakpoint
addi a0, zero, 3
addi a1, zero, 1
call cell_fate
call breakpoint

; If a1={0,1} or a0>=8 then arguments are invalid

ret

test_find_neighbors:

addi sp, sp, -4
stw ra, 0 (sp)

;Exhaustives

addi a0, zero, 0b0100000001001
addi a1, zero, 0
call set_gsa
addi a0, zero, 0b000000000000
addi a1, zero, 1
call set_gsa
addi a0, zero, 0b000001000001
addi a1, zero, 2
call set_gsa
addi a0, zero, 0b000000000000
addi a1, zero, 3
call set_gsa
addi a0, zero, 0b100000000000
addi a1, zero, 4
call set_gsa
addi a0, zero, 0b000000000000
addi a1, zero, 5
call set_gsa
addi a0, zero, 0b000000000000
addi a1, zero, 6
call set_gsa
addi a0, zero, 0b100001000001
addi a1, zero, 7
call set_gsa

call breakpoint

;  100000001001  => 809
;  000000000000  => 000
;  000001000001  => 041
;  000000000000  => 000
;  100000000000  => 800
;  000000000000  => 000
;  000000000000  => 000
;  100001000001  => 841

; Single neighbor search
; 1 neighbor, 1th, no wrap and dead
addi a0, zero, 11-6
addi a1, zero, 3
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0
; 1 neighbor, 2th, no wrap and dead
addi a0, zero, 11-5
addi a1, zero, 3
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0
; 1 neighbor, 3th, no wrap and dead
addi a0, zero, 11-4
addi a1, zero, 3
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0
; 1 neighbor, 4th, no wrap and dead
addi a0, zero, 11-6
addi a1, zero, 2
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0
; 1 neighbor, 5th, no wrap and dead
addi a0, zero, 11-4
addi a1, zero, 2
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0
; 1 neighbor, 6th, no wrap and dead
addi a0, zero, 11-6
addi a1, zero, 1
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0
; 1 neighbor, 7th, no wrap and dead
addi a0, zero, 11-5
addi a1, zero, 1
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0
; 1 neighbor, 8th, no wrap and dead
addi a0, zero, 11-4
addi a1, zero, 1
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0

; positive y wrap
; 1 neighbor, 6th, vertical positive wrap and dead
addi a0, zero, 11-9
addi a1, zero, 7
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0
; 1 neighbor, 7th, vertical positive wrap and dead
addi a0, zero, 11-8
addi a1, zero, 7
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0
; 1 neighbor, 8th, vertical positive wrap and dead
addi a0, zero, 11-7
addi a1, zero, 7
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0

; negative y wrap
; 1 neighbor, 1th, vertical negative wrap and dead
addi a0, zero, 11-6
addi a1, zero, 0
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0
; 1 neighbor, 2th, vertical negative wrap and dead
addi a0, zero, 11-5
addi a1, zero, 0
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0
; 1 neighbor, 3th, vertical negative wrap and dead
addi a0, zero, 11-4
addi a1, zero, 0
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0

; positive x wrap
; 1 neighbor, 3th, horizontal positive and dead
addi a0, zero, 11-11
addi a1, zero, 5
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0
; 1 neighbor, 5th, horizontal positive wrap and dead
addi a0, zero, 11-11
addi a1, zero, 4
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0
; 1 neighbor, 8th, horizontal positive wrap and dead
addi a0, zero, 11-11
addi a1, zero, 3
call find_neighbors
call breakpoint
;Expecting v0=2 v1=0

;negative x wrap
; 1 neighbor, 1th, horizontal negative wrap and dead
addi a0, zero, 11-0
addi a1, zero, 3
call find_neighbors
call breakpoint
;Expecting v0=2 v1=0
; 1 neighbor, 4th, horizontal negative wrap and dead
addi a0, zero, 11-0
addi a1, zero, 2
call find_neighbors
call breakpoint
;Expecting v0=1 v1=0
; 1 neighbor, 8th, horizontal negative wrap and dead
addi a0, zero, 11-0
addi a1, zero, 1
call find_neighbors
call breakpoint
;Expecting v0=3 v1=0

;negative y wrap, negative x wrap and alive and negative y wrap and negative x wrap
; 3 neighbors, 1th 2th 4th and alive
addi a0, zero, 11-0
addi a1, zero, 0
call find_neighbors
call breakpoint
;Expected v0=3 v1=1

;negative y wrap, positive x wrap and negative y wrap and positive x wrap and alive
; 3 neighbors, 2th 3th 5th and alive
addi a0, zero, 11-11
addi a1, zero, 0
call find_neighbors
call breakpoint
;Expected v0=3 v1=1

;positive y wrap, negative x wrap and alive
; 3 neighbors, 4th 6th 7th and alive
addi a0, zero, 11-0
addi a1, zero, 7
call find_neighbors
call breakpoint
;Expected v0=3 v1=1

;positive y wrap, positive x wrap and alive
; 3 neighbors, 5th 7th 8th and alive
addi a0, zero, 11-11
addi a1, zero, 7
call find_neighbors
call breakpoint
;Expected v0=3 v1=1

addi sp, sp, -4
stw ra, 0 (sp)

ret


test_update_gsa:

addi sp, sp, -4
stw ra, 0 (sp)

addi a0, zero, 0b000000000000
addi a1, zero, 0
call set_gsa
addi a0, zero, 0b000000010000
addi a1, zero, 1
call set_gsa
addi a0, zero, 0b000000100000
addi a1, zero, 2
call set_gsa
addi a0, zero, 0b000000111000
addi a1, zero, 3
call set_gsa
addi a0, zero, 0b000000000000
addi a1, zero, 4
call set_gsa
addi a0, zero, 0b000000000000
addi a1, zero, 5
call set_gsa
addi a0, zero, 0b000000000000
addi a1, zero, 6
call set_gsa
addi a0, zero, 0b000000000000
addi a1, zero, 7
call set_gsa
call clear_leds
call draw_gsa
call breakpoint


; 000000000000
; 000010000000
; 000001000000
; 000111000000
; 000000000000
; 000000000000
; 000000000000
; 000000000000

call update_gsa
call clear_leds
call draw_gsa
call breakpoint

; Expected 
; 000000000000
; 000000000000
; 000101000000
; 000111000000
; 000000000000
; 000000000000
; 000000000000
; 000000000000

call update_gsa
call clear_leds
call draw_gsa
call breakpoint

; Expected
; 000000000000
; 000000000000
; 000001000000
; 000101000000
; 000011000000
; 000000000000
; 000000000000
; 000000000000


call breakpoint
addi a0, zero, 0b000000000000
addi a1, zero, 0
call set_gsa
addi a0, zero, 0b000000000000
addi a1, zero, 1
call set_gsa
addi a0, zero, 0b000001011100
addi a1, zero, 2
call set_gsa
addi a0, zero, 0b000001000000
addi a1, zero, 3
call set_gsa
addi a0, zero, 0b001001000000
addi a1, zero, 4
call set_gsa
addi a0, zero, 0b001000000000
addi a1, zero, 5
call set_gsa
addi a0, zero, 0b001000001110
addi a1, zero, 6
call set_gsa
addi a0, zero, 0b000000000000
addi a1, zero, 7
call set_gsa
call clear_leds
call draw_gsa
call breakpoint


; 000000000000
; 000000000000
; 001110100000
; 000000100000
; 000000100100
; 000000000100
; 011100000100
; 000000000000

call update_gsa
call update_gsa
call update_gsa
call update_gsa
call update_gsa
call clear_leds
call draw_gsa
call breakpoint

; 000000000000
; 001110000000
; 001001100000
; 001101001100
; 000001110010
; 001000000110
; 001000000000
; 001000000000

call update_gsa
call clear_leds
call draw_gsa
call breakpoint

; 001000000000
; 001111000000
; 010001100000
; 001100001100
; 001111110010
; 000000100110
; 011100000000
; 000000000000

ldw ra, 0 (sp)
addi sp, sp, 4

ret

test_change_step:
addi a0,zero,1
addi a1,zero,0
addi a2,zero,1 ; 3+101=104
call change_steps
addi a0,zero,1
addi a1,zero,0
addi a2,zero,0 ; 104+1=105
call change_steps
addi a0,zero,0
addi a1,zero,1
addi a2,zero,1 ; 105+110=215
call change_steps

test_update_state:

addi sp, sp, -4
stw ra, 0 (sp)

; set CURR_STATE TO INIT (0)
	; press b0
	call breakpoint
	ldw a0, BUTTONS+4 (zero)
	call update_state
	; expect : no change i.e CURR_STATE = 0
	; press b1
	call breakpoint
	ldw a0, BUTTONS+4 (zero)
	call update_state
	; expect : switch to RUN i.e CURR_STATE = 2
	; press b2
	call breakpoint
	ldw a0, BUTTONS+4 (zero)
	call update_state
	; expect : no change i.e CURR_STATE = 0
	; press b3
	call breakpoint
	ldw a0, BUTTONS+4 (zero)
	call update_state
	; expect : no change i.e CURR_STATE = 0
	; press b4
	call breakpoint
	ldw a0, BUTTONS+4 (zero)
	call update_state
	; expect : no change i.e CURR_STATE = 0
; set CURR_STATE TO RAND (1)
	; press b0
	call breakpoint
	ldw a0, BUTTONS+4 (zero)
	call update_state
	; expect : no change i.e CURR_STATE = 1
	; press b1
	call breakpoint
	ldw a0, BUTTONS+4 (zero)
	call update_state
	; expect : switch to RUN i.e CURR_STATE = 2
	; press b2
	call breakpoint
	ldw a0, BUTTONS+4 (zero)
	call update_state
	; expect : no change i.e CURR_STATE = 1
	; press b3
	call breakpoint
	ldw a0, BUTTONS+4 (zero)
	call update_state
	; expect : no change i.e CURR_STATE = 1
	; press b4
	call breakpoint
	ldw a0, BUTTONS+4 (zero)
	call update_state
	; expect : no change i.e CURR_STATE = 1
; set CURR_STATE TO RUN (2) and set CURR_STEP to a non zero positive value
	; press b0
	call breakpoint
	ldw a0, BUTTONS+4 (zero)
	call update_state
	; expect : no change i.e CURR_STATE = 2
	; press b1
	call breakpoint
	ldw a0, BUTTONS+4 (zero)
	call update_state
	; expect : no change i.e CURR_STATE = 2
	; press b2
	call breakpoint
	ldw a0, BUTTONS+4 (zero)
	call update_state
	; expect : no change i.e CURR_STATE = 2
	; press b3
	call breakpoint
	ldw a0, BUTTONS+4 (zero)
	call update_state
	; expect : switch_to_init i.e CURR_STATE = 0 AND CURR_STEPS = 0
	; press b4
	call breakpoint
	ldw a0, BUTTONS+4 (zero)
	call update_state
	; expect : no change i.e CURR_STATE = 2


ldw ra, 0 (sp)
addi sp, sp, 4

ret

test_select_action:
	addi sp, sp, -4
	stw ra, 0 (sp)
	test_action_init_state:
		addi t0, zero, INIT
		stw t0, CURR_STATE (zero)
		call breakpoint
		ldw a0, BUTTONS+4 (zero)
		call select_action
		addi t1, zero, 6
		addi s0, s0, 1
		blt s0, t1, test_action_init_state
		addi s0, zero, 0
	test_action_rand_state:
		addi t0, zero, RAND
		stw t0, CURR_STATE (zero)
		call breakpoint
		ldw a0, BUTTONS+4 (zero)
		call select_action
		addi t1, zero, 5
		addi s0, s0, 1
		blt s0, t1 , test_action_rand_state
		addi s0, zero, 0
	test_action_run_state:
		addi t0, zero, RUN
		stw t0, CURR_STATE (zero)
		call breakpoint
		ldw a0, BUTTONS+4 (zero)
		call select_action
		addi t1, zero, 5
		addi s0, s0, 1
		blt s0, t1 , test_action_run_state
	
	ldw ra, 0 (sp)
	addi sp, sp, 4	
	ret
	
	
test_mask:

addi a0, zero, 0xFFF
addi a1, zero, 0
call set_gsa
addi a0, zero, 0xFFF
addi a1, zero, 1
call set_gsa
addi a0, zero, 0xFFF
addi a1, zero, 2
call set_gsa
addi a0, zero, 0xFFF
addi a1, zero, 3
call set_gsa
addi a0, zero, 0xFFF
addi a1, zero, 4
call set_gsa
addi a0, zero, 0xFFF
addi a1, zero, 5
call set_gsa
addi a0, zero, 0xFFF
addi a1, zero, 6
call set_gsa

call mask



ret



test_change_speed:
addi t0,zero,1
add a0,zero,t0 ; a0=1
call change_speed
addi t0,zero,0
add a0,zero,t0 ;a0=0
call change_speed
addi t0,zero,1
add a0,zero,t0 ; a0=1
call change_speed


; END:helper

