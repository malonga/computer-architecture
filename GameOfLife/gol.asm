
     ;;    game state memory location
    .equ CURR_STATE, 0x1000              ; current game state
    .equ GSA_ID, 0x1004                     ; gsa currently in use for drawing
    .equ PAUSE, 0x1008                     ; is the game paused or running
    .equ SPEED, 0x100C                      ; game speed
    .equ CURR_STEP,  0x1010              ; game current step
    .equ SEED, 0x1014              ; game seed
    .equ GSA0, 0x1018              ; GSA0 starting address
    .equ GSA1, 0x1038              ; GSA1 starting address
    .equ SEVEN_SEGS, 0x1198             ; 7-segment display addresses
    .equ CUSTOM_VAR_START, 0x1200 ; Free range of addresses for custom variable definition
    .equ CUSTOM_VAR_END, 0x1300
    .equ LEDS, 0x2000                       ; LED address
    .equ RANDOM_NUM, 0x2010          ; Random number generator address
    .equ BUTTONS, 0x2030                 ; Buttons addresses

    ;; states
    .equ INIT, 0
    .equ RAND, 1
    .equ RUN, 2

    ;; constants
    .equ N_SEEDS, 4
    .equ N_GSA_LINES, 8
    .equ N_GSA_COLUMNS, 12
    .equ MAX_SPEED, 10
    .equ MIN_SPEED, 1
    .equ PAUSED, 0x00
    .equ RUNNING, 0x01

main:
    addi sp, zero, 0x2000
	call normal_main
    ret
    ;; TODO

font_data:
    .word 0xFC ; 0
    .word 0x60 ; 1
    .word 0xDA ; 2
    .word 0xF2 ; 3
    .word 0x66 ; 4
    .word 0xB6 ; 5
    .word 0xBE ; 6
    .word 0xE0 ; 7
    .word 0xFE ; 8
    .word 0xF6 ; 9
    .word 0xEE ; A
    .word 0x3E ; B
    .word 0x9C ; C
    .word 0x7A ; D
    .word 0x9E ; E
    .word 0x8E ; F

seed0:
    .word 0xC00
    .word 0xC00
    .word 0x000
    .word 0x060
    .word 0x0A0
    .word 0x0C6
    .word 0x006
    .word 0x000

seed1:
    .word 0x000
    .word 0x000
    .word 0x05C
    .word 0x040
    .word 0x240
    .word 0x200
    .word 0x20E
    .word 0x000

seed2:
    .word 0x000
    .word 0x010
    .word 0x020
    .word 0x038
    .word 0x000
    .word 0x000
    .word 0x000
    .word 0x000

seed3:
    .word 0x000
    .word 0x000
    .word 0x090
    .word 0x008
    .word 0x088
    .word 0x078
    .word 0x000
    .word 0x000

    ;; Predefined seeds
SEEDS:
    .word seed0
    .word seed1
    .word seed2
    .word seed3

mask0:
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF

mask1:
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0x1FF
    .word 0x1FF
    .word 0x1FF

mask2:
    .word 0x7FF
    .word 0x7FF
    .word 0x7FF
    .word 0x7FF
    .word 0x7FF
    .word 0x7FF
    .word 0x7FF
    .word 0x7FF

mask3:
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0x000

mask4:
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0xFFF
    .word 0x000

MASKS:
    .word mask0
    .word mask1
    .word mask2
    .word mask3
    .word mask4

; BEGIN:helper

normal_main:
addi sp, sp, -12
stw ra, 0 (sp)
stw s0,4(sp)
stw s1,8(sp)

call reset_game
call get_input
addi s0, v0, 0
addi s1, zero, 0
game_not_done:
addi a0, s0, 0
call select_action
addi a0, s0, 0
call update_state
call update_gsa
call mask
call clear_leds
call draw_gsa
call wait
call decrement_step
addi s1, v0, 0
call get_input
addi s0, v0, 0
beq s1, zero, game_not_done
beq zero,zero,normal_main

ldw s1,8(sp)
ldw s0,4(sp)
ldw ra,0(sp)
addi sp, sp, 12

ret


breakpoint:
ret


assemble_gsa_line_address: ; Puts the address of the GSA line a0 into v0
; retrieve GSA_ID
ldw v0, GSA_ID (zero) ; v0 is GSA_ID

; get address of GSA into t1
addi t1, zero, GSA0
beq v0, zero, t1_is_correct
addi t1, zero, GSA1
t1_is_correct: ; v0 is address of GSA in use t0 has served its purpose

; get line specific variation
slli t0, a0, 2 ; t0 line specific variation of address

; assemble address
add v0, t1, t0 ; t0 address of the specific line needed

ret

update_display:
ldw t4, CURR_STEP (zero)

andi t3,t4,0xF ; units
slli t3,t3,2 ; multiply by 4
ldw t3, font_data(t3)

andi t2,t4,0xF0 ; tens
srli t2,t2,4
slli t2,t2,2; multiply by 4
ldw t2 , font_data(t2)

andi t1,t4,0xF00 ; hundreds
srli t1,t1,8
slli t1,t1,2 ; multiply by 4
ldw t1, font_data(t1)

ldw t0, font_data(zero)

stw t0, SEVEN_SEGS(zero)
stw t1, SEVEN_SEGS+4 (zero)
stw t2, SEVEN_SEGS+8 (zero)
stw t3, SEVEN_SEGS+12 (zero)

ret

; END:helper



; BEGIN:clear_leds
clear_leds:

addi t0 , zero, LEDS ;t0 = LEDS
stw zero, 0 (t0) ;LEDS[0]=0
stw zero, 4 (t0) ;LEDS[1]=0
stw zero, 8 (t0) ;LEDS[2]=0

ret
; END:clear_leds

; BEGIN:set_pixel
set_pixel:

; Determine address to change
srli t0, a0, 2 ; Determine what LED word to change | t0->variation address of LEDS word
slli t0, t0, 2 ; Multiply by 4 so that it corresponds to address variation | t0=variation address of LEDS word

; Determine what position to change
andi t1, a0, 3 ; Get x%4 | t1->index to change in word
slli t1, t1, 3 ; Multiply x%4 by 8 | t1->index to change in word
add t1, t1, a1 ; Add y | t1=index to change in word

; Create the corresponding mask
addi t2, zero, 1  ; | t2 -> ormask
sll t2, t2, t1 ; |t2=ormask t1 has served its purpose as index

; Get copy of current state of LEDS word to change
ldw t1, LEDS (t0) ; | t1 = state of the LEDS word

; or current_state with mask of pixel to light
or t1, t1, t2 ;| t1=future state of the LEDS word t2 has served its purpose

; replace LEDS[] with result
stw t1, LEDS (t0)

ret

; END:set_pixel

; BEGIN:wait
wait:

addi t0, zero, 1 ; | 
slli t0, t0, 20 ;  | t0 = loop counter
ldw t1, SPEED (zero) ; Retrieve the game speed | t1 = loop step
decrement_loop:
    sub t0, t0, t1
    blt t1, t0, decrement_loop

ret
; END:wait

; BEGIN:get_gsa
get_gsa:

addi sp, sp, -4
stw ra, 0 (sp)

call assemble_gsa_line_address

ldw v0, 0 (v0) ; Read line and put it into v0

ldw ra, 0 (sp)
addi sp, sp, 4

ret
; END:get_gsa

; BEGIN:set_gsa
set_gsa:

;save_s0
addi sp, sp, -8
stw s0, 0 (sp)
stw ra, 4 (sp)

add s0, zero, a0 ; s0 = the content of the line to be written
add a0, a1, zero ; a0 = the y coordinate

call assemble_gsa_line_address
stw s0, 0 (v0)

;retrieve s0
ldw ra, 4 (sp)
ldw s0, 0 (sp)
addi sp, sp, 8

ret
; END:set_gsa

; BEGIN:draw_gsa
draw_gsa:
;Manage stack
addi sp, sp, -16
stw s0, 0 (sp)
stw s1, 4 (sp)
stw s2, 8 (sp)
stw ra, 12 (sp)

addi s0, zero, 7 ; s0 = the line we are currently printing 

print_lines:
add a0, zero, s0 ; load a0 with line we want to read
call get_gsa ; get into v0 content of line a0 == s0
add s2, v0, zero ; s2 = content of GSA at line s0
addi s1, zero, 0 ; s1 = x coordinate of the pixel we are currently printing

print_pixels:
andi t0, s2, 1 ; t0 = wether the current pixel should be lit
beq t0, zero, pixel_is_set ; if the current pixel must not be lit, skip the lighting process
add a0, zero, s1 ; Load a0 with x-coordinate
add a1, zero, s0 ; Load a1 with y-coordinate
call set_pixel ; 
pixel_is_set:
addi t1,zero,N_GSA_COLUMNS -1 ; t1=11
addi s1, s1, 1 ; s1 ++ i.e print next_pixel
srli s2, s2, 1 ; =>  we need to read the next bit for the next pixel
bge t1, s1, print_pixels ; if 11>=s1 then go back to print_pixels

addi s0, s0, -1 ; decrement the line to print
bge s0, zero, print_lines ; if s0>= then go back to print_lines

;Manage stack
ldw s0, 0 (sp)
ldw s1, 4 (sp)
ldw s2, 8 (sp)
ldw ra, 12 (sp)
addi sp, sp, 16

ret
; END:draw_gsa

; BEGIN:random_gsa
random_gsa:
;Manage stack (moved up)
addi sp, sp, -8 ; 
stw ra, 4 (sp)
stw s0, 0 (sp)

add s0,zero,zero ; s0 = line being made
lloop: 
; retrieve RANDOM_NUM
ldw a0, RANDOM_NUM (zero) ; a0 = the content of the line
andi a0, a0, 0x0FFF ; the 20 MSB must be zero 0b11..1 (with 12 1s) = 0x0FFF
slli t1, s0, 2
ldw t0, mask4(t1) ; Load mask for current line
and a0, a0, t0 ; and with wall mask
add a1, zero, s0 ; a1 = y-coordinate
call set_gsa
addi s0, s0, 1 ; s0 ++
addi t0 , zero, N_GSA_LINES ; Load literal value (8) into registers
blt s0, t0 , lloop ; loop excecuted 8 times | Before ble t0, 7, lloop

;Manage stack
ldw s0, 0 (sp)
ldw ra, 4 (sp)
addi sp, sp, 8

ret
; END:random_gsa


; BEGIN:change_speed
change_speed:

; can't compare literal with register values
ldw t0, SPEED (zero) ; load speed into t0, so that it may be used in comparison | t0 = SPEED

beq a0, zero, increase_speed
; can't compare literal with register values since only two cases, no need for comparison Before : beq a0,1,decrease_speed

addi t1, zero, MIN_SPEED ; MIN_SPEED=1
beq t0, t1,speed_is_min ;ret isn't a label, can only branch to label

addi t0, t0 ,-1 ; decrement t0 
stw t0, SPEED (zero) ; write t0 at speed
ret

increase_speed:
;load MAX_SPEED into t1 so that it may be used in comparison
addi t1, zero, MAX_SPEED
bge t0, t1, speed_is_correct
addi t0, t0, 1 ; increment t0
stw t0, SPEED (zero) ; write t0 at speed

speed_is_min:
speed_is_correct:

ret
; END:change_speed


; BEGIN:pause_game
pause_game:

ldw t0, PAUSE (zero)
xori t2,t0,1
stw t2, PAUSE (zero)

ret
; END:pause_game


; BEGIN:change_steps
change_steps:
ldw t0, CURR_STEP (zero)

beq a0, zero, units_done
addi t0, t0, 1 ; add 1 to units
units_done:

beq a1, zero, tens_done
addi t0, t0, 0x10 ; add 1 to tens
tens_done:

beq a2, zero, hundreds_done
addi t0, t0, 0x100
hundreds_done:

;assemble result
andi t0, t0, 0xFFF 
stw t0, CURR_STEP (zero)

ret
; END:change_steps


; BEGIN:increment_seed
increment_seed:

addi sp, sp, -4
stw ra, 0 (sp)

ldw t0, CURR_STATE (zero)
addi t1, zero, INIT
bne t0, t1, not_init
;state is init

ldw t1, SEED (zero)
addi t3,zero,N_SEEDS
bne t1,t3,continue
;si = 4
call random_gsa
ldw ra, 0 (sp)
addi sp, sp, 4
ret

continue:
ldw t1, SEED (zero)
addi t1, t1, 1
stw t1, SEED (zero)

ldw t1, SEED (zero)
addi t3,zero,N_SEEDS
beq t1,t3,random
br apply_seed
;beq t1, t2, apply_seed
ldw ra, 0 (sp)
addi sp, sp, 4
ret

random:
call random_gsa
ldw ra, 0 (sp)
addi sp, sp, 4
ret

not_init:
;state is not init
addi t1, zero, RAND
bne t0, t1, not_rand
call random_gsa

not_rand:
ldw ra, 0 (sp)
addi sp, sp, 4
ret

apply_seed:
addi sp, sp, -12
stw s0, 0 (sp)
stw s1, 4 (sp)
stw s2, 8 (sp)

ldw t0, SEED (zero)
slli t0, t0, 2
add s1, zero, zero ; s1 = numÃ©ro de la ligne en cours de traitement
ldw s2, MASKS (t0) ; s2 = addresse globale du mask dont on a besoin
ldw s0, SEEDS (t0) ; s0 = addresse globale du seed dont on a besoin
apply_seed_line:

slli t1, s1, 2 ; addresse locale de la ligne
add t0, s0, t1 ; addresse globale du mot de seed deÃŸ la ligne
add t2, s2, t1 ; addresse globale du mot de mask de la ligne
ldw t0, 0 (t0) ; t0 = ligne de seed
ldw t2, 0 (t2) ; t2 = ligne de mask
and t0, t0, t2 ; contenu de la ligne masque

addi a0, t0, 0 ; Mise du resultat dans a0
add a1, s1, zero ; la ligne 
call set_gsa
addi s1, s1, 1
addi t0, zero, N_GSA_LINES
blt s1, t0, apply_seed_line

ldw s0, 0 (sp)
ldw s1, 4 (sp)
ldw s2, 8 (sp)
addi sp, sp, 12
ldw ra, 0 (sp)
addi sp, sp, 4

ret
; END:increment_seed


; BEGIN:update_state
update_state:

addi sp, sp, -8
stw ra, 0 (sp)
stw s0, 4 (sp)

add s0, zero, a0
ldw t0, CURR_STATE(zero)
addi t1, zero, RUN
bne t0, t1, us_not_running

andi t1, s0, 0b01000
bne t1, zero, switch_to_init
br no_update

stw t0,CURR_STEP(zero)
beq t0,zero,switch_to_init

us_not_running:

addi t1, zero, N_SEEDS
ldw t2, SEED (zero)
blt t2, t1, check_b1
;addi t1, zero, RAND
;ldw t2, CURR_STATE (zero)
;bne t1, t2, switch_to_rand
br switch_to_rand;;;;;;;;;;;;;;;;;;;;;

check_b1:
andi t1, s0, 0b10
bne t1, zero, switch_to_run

br no_update

switch_to_rand:
addi t0, zero, RAND
stw t0, CURR_STATE (zero)
addi t0, zero, N_SEEDS
stw t0, SEED (zero)
br check_b1

switch_to_run:
addi t0, zero, RUN
stw t0, CURR_STATE (zero)
call pause_game
br no_update

switch_to_init:
call reset_game
br no_update


no_update:

;Manage stack
ldw ra, 0 (sp)
ldw s0, 4 (sp)
addi sp, sp, 8

ret
; END:update_state


; BEGIN:select_action
select_action:

beq a0, zero, nothing_to_do

; Manage stack
addi sp, sp, -8
stw ra, 0 (sp)
stw s0, 4 (sp)

addi s0, a0, 0

ldw t0, CURR_STATE (zero)
addi t1, zero, RUN
bne t0, t1, not_running

andi t2,s0,0b1
beq t2, zero,dont_pause
call pause_game
br sa_done

dont_pause:

andi t0, s0, 0b110
beq t0, zero, b3_served
andi t0, t0, 0b10
srli a0, t0, 1
xori a0, a0, 1
call change_speed
br sa_done

b3_served:
andi t0, s0, 0b10000
beq t0, zero, sa_done
call random_gsa
br sa_done


not_running:

andi t0, s0, 1
beq t0, zero, no_inc
call increment_seed
no_inc:
andi a0, s0, 0b10000
srli a0, a0, 4
andi a1, s0, 0b1000
srli a1, a1, 3
andi a2, s0, 0b100
srli a2, a2, 2
call change_steps

sa_done:
; Manage stack
ldw ra, 0 (sp)
ldw s0, 4 (sp)
addi sp, sp, 8

nothing_to_do:

ret

; END:select_action


; BEGIN:cell_fate
cell_fate:
; From rules : ALIVE = (a1==1)and(a0>=2)and(a0<4)OR(a1==0)and(a0==3) --> 
; a1 == (a1==1)
; (a1 -1) AND 1 == (a1 == 0)
; "lazy"-val : let's split case a1==1 and a1==0
beq a1, zero, cell_was_dead
cmpgei t0, a0, 2 ; t0 = (a0>=2)
cmplti t1, a0, 4 ; t1 = (a0<4)
and v0, t0, t1 
ret
cell_was_dead:
cmpeqi v0, a0, 3
ret
; END:cell_fate

; BEGIN:find_neighbours
find_neighbours:

;manage stack
addi sp, sp, -24
stw s0, 0 (sp)
stw s1, 4 (sp)
stw s2, 8 (sp)
stw s3, 12 (sp)
stw s4, 16 (sp)
stw ra, 20 (sp)


add s0, zero, a0 ; s0 = x
add s1, zero, a1 ; s1 = y

; left neighbours
addi a0, s1, -1 ; a0 = y-1
andi a0, a0, 7 ; a0 = (y-1) mod 8
call get_gsa
add s2, v0, zero ; s2 = GSA element y-1
add a0, s1, zero ; a0 = y
call get_gsa
add s3, v0, zero ; s3 = GSA element y
addi a0, s1, 1 ; a0 = y+1
andi a0, a0, 7 ; a0 = (y+1) mod 8 
call get_gsa
add s4, v0, zero ; s4 = GSA element y+1

addi t0, s0, -1
bge t0, zero, xminus1_set
addi t0, t0, N_GSA_COLUMNS
xminus1_set:
srl t2, s2, t0 
srl t3, s3, t0
srl t4, s4, t0
andi t2, t2, 1 ; t2 = whether x-1 y-1 is alive
andi t3, t3, 1 ; t3 = whether x-1 y is alive
andi t4, t4, 1 ; t4 = whether x-1 y+1 is alive

add t2, t2, t3 
add t1, t2, t4 ; t1 = number of neighbors on the left
add v0, t1, zero ; v0 = number of neighbors on the left

; right neighbours
addi t0, s0, 1
addi t2, zero, N_GSA_COLUMNS
blt t0, t2, xplus1_set
add t0, zero, zero
xplus1_set:
srl t2, s2, t0 
srl t3, s3, t0
srl t4, s4, t0
andi t2, t2, 1 ; t2 = whether x+1 y-1 is alive
andi t3, t3, 1 ; t3 = whether x+1 y is alive
andi t4, t4, 1 ; t4 = whether x+1 y+1 is alive

add t2, t2, t3 
add t1, t2, t4 ; t1 = number of neighbors on the right
add v0, v0, t1 ; v0 = number of neighbors on left and right

; same x neighbours
srl t2, s2, s0 
srl t3, s3, s0
srl t4, s4, s0
andi t2, t2, 1 ; t2 = whether x+1 y-1 is alive
andi v1, t3, 1
andi t4, t4, 1 ; t4 = whether x+1 y+1 is alive

add t1, t2, t4 ; number of neighbors at x

add v0, t1, v0


ldw s0, 0 (sp)
ldw s1, 4 (sp)
ldw s2, 8 (sp)
ldw s3, 12 (sp)
ldw s4, 16 (sp)
ldw ra, 20 (sp)
addi sp, sp, 24

ret
; END:find_neighbours


; BEGIN:update_gsa
update_gsa:
ldw t0, PAUSE (zero)
addi t1, zero, PAUSED
beq t0,t1,game_is_paused ; if the game is paused, we can't update

;manage stack
addi sp, sp, -16 
stw s0, 0 (sp)
stw ra, 4 (sp)
stw s1, 8 (sp)
stw s2, 12 (sp)

addi s0,zero,0 ; x coor
addi s1, zero, 0 ; y coor
addi s2, zero, 0 ; line being constructed

new_fate:

add a0,zero,s0  ; x coor
add a1,zero,s1 ; y coor
call find_neighbours ; v0 the nb of living neighbours

;cell_fate call
add a1,zero,v1 ; examined cell state
add a0,zero,v0 ; nb of living neighbours
call cell_fate
add t0,zero,v0 ; new cell state from cell_fate

;update the gsa line
sll t0, t0, s0
or s2, s2, t0

;increment x
addi s0,s0,1

;iterate on the next cell of the same line
addi t1,zero,N_GSA_COLUMNS
blt s0,t1,new_fate ; if s0 <12, go to next cell
;line is done 
;store the line :
;invert GSA_ID
ldw t0, GSA_ID (zero)
xori t0, t0, 1
stw t0, GSA_ID (zero)
;store
addi a0, s2, 0
addi a1, s1, 0
call set_gsa
;invert GSA_ID back into original state
ldw t0, GSA_ID (zero)
xori t0, t0, 1
stw t0, GSA_ID (zero)
;get ready for the next line
addi s0, zero, 0 ; x=0
addi s1, s1, 1 ; y++
addi s2, zero, 0 ; line is empty
addi t1, zero, N_GSA_LINES
blt s1, t1, new_fate ; if y<8, i.e not all lines are done 

;invert GSA_ID
ldw t0, GSA_ID (zero)
xori t0, t0, 1
stw t0, GSA_ID (zero)


ldw s0, 0 (sp)
ldw ra, 4 (sp)
ldw s1, 8 (sp)
ldw s2, 12 (sp)
addi sp, sp, 16 ;

game_is_paused:

ret
; END:update_gsa


; BEGIN:mask
mask:

; Manage stack
addi sp, sp, - 16
stw s0, 0 (sp)
stw s1, 4 (sp)
stw ra, 8 (sp)
stw s2, 12 (sp)

ldw s0, SEED (zero) ; s0 = seed
slli t0, s0, 2 ; t0 = seed * 4
ldw s0, MASKS (t0) ; s0 = address of mask nÂ° seed
ldw t0, CURR_STATE (zero)
addi t1, zero, RAND
bne t0, t1, mask_state_is_not_rand
addi t0, zero, N_SEEDS
slli t0, t0, 2
ldw s0, MASKS (t0) ; s0 = address of mask nÂ° N_SEEDS
mask_state_is_not_rand: ; s0 = address of mask in use

addi s1, zero, 0 ; y 

mask_line:
addi a0, s1, 0
slli t0, s1, 2 ; t0 = y*4
add t2, s0, t0 ; t2 = address of mask line
ldw s2, 0 (t2) ; s2 = line y of the mask
call get_gsa
addi t0, v0, 0 ; t0 = GSA element y
and t0, t0, s2 ; t0 = GSA element y AND mask of line y
addi a0, t0, 0 ; load content of line y into a0
addi a1, s1, 0 ; load line number into a1
call set_gsa
addi t0, zero, N_GSA_LINES
addi s1, s1, 1 ; y++
blt s1, t0, mask_line

;Manage stack
ldw s0, 0 (sp)
ldw s1, 4 (sp)
ldw ra, 8 (sp)
ldw s2, 12 (sp)
addi sp, sp, 16

ret
; END:mask


; BEGIN:get_input
get_input:

addi t1,zero,4
ldw t0, BUTTONS (t1) ; starts loading from address 4
add v0,t0,zero
stw zero, BUTTONS (t1) ; clear the edgecapture
ret

; END:get_input


; BEGIN:decrement_step
decrement_step:
addi sp, sp, -24
stw ra, 0 (sp)
stw s0,4(sp)
stw s1,8(sp)
stw s2,12(sp)
stw s3,16(sp)
stw s4,20(sp)

; store states in registers
ldw s0, CURR_STATE (zero)
addi s4, zero, RUN
beq s0, s4, state_is_run 

;RAND or INIT
add v0, zero, zero ; returns 0
br disp_and_end

state_is_run:

addi s2, zero, PAUSED
ldw s3, PAUSE (zero)
add v0,zero,zero ; returns 0
beq s2, s3, disp_and_end

ldw s1, CURR_STEP (zero)
bne s1, zero, state_is_run_and_more_steps ;if the current step is not 0 => decrements nb of steps
addi v0,zero,1 ;current step is 0 => return 1
br disp_and_end

state_is_run_and_more_steps:

addi s1,s1,-1 ; t0=nb of steps - 1
stw s1, CURR_STEP (zero)
add v0,zero,zero ; returns 0

disp_and_end:
call update_display

ldw s4,20(sp)
ldw s3,16(sp)
ldw s2,12(sp)
ldw s1,8(sp)
ldw s0,4(sp)
ldw ra, 0 (sp)
addi sp, sp, 24
ret
; END:decrement_step



; BEGIN:reset_game
reset_game:

addi sp, sp, -24
stw ra, 0 (sp)
stw s0, 4(sp)
stw s1,8(sp)
stw s2,12(sp)
stw s3,16(sp)
stw s4,20(sp)

;the game is currently pause
addi s0,zero, PAUSED
stw s0, PAUSE (zero)

;current sate is INIT
addi s1,zero,INIT
stw s1, CURR_STATE(zero)

;current step is 1 + on display
addi s2,zero,1
stw s2, CURR_STEP(zero) ; store 1 in current step
call update_display

;gsa ID is 0
stw zero, GSA_ID (zero)

;seed 0 is selected
addi s3,zero,-1
stw s3, SEED (zero) ; store seed0 in the game as the game seed
call increment_seed

;clear_leds
call clear_leds

;draw the gsa of seed 0
call draw_gsa


;the game speed is 1
addi s4,zero,1
stw s4, SPEED (zero)

ldw s4,20(sp)
ldw s3,16(sp)
ldw s2,12(sp)
ldw s1,8(sp)
ldw s0, 4(sp)
ldw ra, 0 (sp)
addi sp, sp, 24
ret
; END:reset_game