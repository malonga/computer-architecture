library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PC is
    port(
        clk     : in  std_logic;
        reset_n : in  std_logic;
        en      : in  std_logic;
        sel_a   : in  std_logic;
        sel_imm : in  std_logic;
        add_imm : in  std_logic;
        imm     : in  std_logic_vector(15 downto 0);
        a       : in  std_logic_vector(15 downto 0);
        addr    : out std_logic_vector(31 downto 0)
    );
end PC;

architecture synth of PC is
	SIGNAL s_addr :std_logic_vector(15 downto 0) := (others=>'0'); 
begin

update: process(clk,reset_n) is
begin

	if(reset_n='0') then
		s_addr<=(others=>'0');
	
	elsif (rising_edge(clk) AND reset_n='1' AND en='1') then
		
		if (sel_imm='1') then --select imm 
			s_addr(15 downto 0)<=std_logic_vector(unsigned(imm) sll 2) ; --shifted to the left by 2 
		elsif (sel_a='1') then --select a
			s_addr(15 downto 0)<=a;	
		elsif(add_imm='1') then --add imm
			s_addr<=std_logic_vector(signed(s_addr) +  signed(imm));
		else --add 4
			s_addr<=std_logic_vector(signed(s_addr) + to_signed(4,16));
		end if;
	end if;
	
end process;
	
addr <= (15 downto 0 => '0')&s_addr(15 downto 2)&"00"; 

end synth;
