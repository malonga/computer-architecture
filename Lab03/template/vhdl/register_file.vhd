library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity register_file is
    port(
        clk    : in  std_logic;
        aa     : in  std_logic_vector(4 downto 0);
        ab     : in  std_logic_vector(4 downto 0);
        aw     : in  std_logic_vector(4 downto 0);
        wren   : in  std_logic;
        wrdata : in  std_logic_vector(31 downto 0);
        a      : out std_logic_vector(31 downto 0);
        b      : out std_logic_vector(31 downto 0)
    );
end register_file;

architecture synth of register_file is

	subtype REG is std_logic_vector(31 downto 0);
	type REGISTERS is array(0 to 31) of REG;
	signal register_file : REGISTERS := (others=>(others=>'0'));

begin
-- Read processes
	-- read process a
	a <= register_file(to_integer(unsigned(aa)));
	-- read process b
	b <= register_file(to_integer(unsigned(ab)));
	
-- Write process
	Write : process(clk)
	begin
		if (rising_edge(clk) and wren='1') then
			register_file(to_integer(unsigned(aw)))<=wrdata;
			register_file(0)<=(31 downto 0 => '0' );
		end if;

	end process;
end synth;
