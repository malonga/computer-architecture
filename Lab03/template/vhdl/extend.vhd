library ieee;
use ieee.std_logic_1164.all;

entity extend is
    port(
        imm16  : in  std_logic_vector(15 downto 0);
        signed : in  std_logic;
        imm32  : out std_logic_vector(31 downto 0)
    );
end extend;

architecture synth of extend is

signal s_imm32: std_logic_vector(31 downto 0);

begin

s_imm32(31 downto 16) <= (31 downto 16 => '0') when signed='0'
							else (31 downto 16 => imm16(15));
s_imm32(15 downto 0) <= imm16;
	
imm32<=s_imm32;


end synth;
