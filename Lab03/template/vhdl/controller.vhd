library ieee;
use ieee.std_logic_1164.all;

entity controller is
    port(
        clk        : in  std_logic;
        reset_n    : in  std_logic;
        -- instruction opcode
        op         : in  std_logic_vector(5 downto 0);
        opx        : in  std_logic_vector(5 downto 0);
        -- activates branch condition
        branch_op  : out std_logic;
        -- immediate value sign extention
        imm_signed : out std_logic;
        -- instruction register enable
        ir_en      : out std_logic;
        -- PC control signals
        pc_add_imm : out std_logic;
        pc_en      : out std_logic;
        pc_sel_a   : out std_logic;
        pc_sel_imm : out std_logic;
        -- register file enable
        rf_wren    : out std_logic;
        -- multiplexers selections
        sel_addr   : out std_logic;
        sel_b      : out std_logic;
        sel_mem    : out std_logic;
        sel_pc     : out std_logic;
        sel_ra     : out std_logic;
        sel_rC     : out std_logic;
        -- write memory output
        read       : out std_logic;
        write      : out std_logic;
        -- alu op
        op_alu     : out std_logic_vector(5 downto 0)
    );
end controller;

architecture synth of controller is
TYPE All_States is (FETCH_1, FETCH_2, BREAK, DECODE, R_OP, STORE, I_OP, IU_OP, RI_OP, BRANCH, CALL, CALLR, JMP, JMPI, LOAD_1, LOAD_2); 
-- SUBTYPE Execute_states IS State RANGE 4 TO 10/11 should we include both Loads, only the one that follows decode or only the one which precedes Fetch1 2345
SIGNAL State : All_States := FETCH_1;

signal i_op_alu:std_logic_vector(5 downto 0);
signal iu_op_alu:std_logic_vector(5 downto 0);
signal r_op_alu:std_logic_vector(5 downto 0);
signal ri_op_alu:std_logic_vector(5 downto 0);
signal br_op_alu:std_logic_vector(5 downto 0);
signal s_op_alu:std_logic_vector(5 downto 0);


signal in_execute_state : std_logic;
signal is_R_type : std_logic;

begin

i_op_alu(2 downto 0) <= op(5 downto 3);
iu_op_alu(2 downto 0) <= op(5 downto 3);
br_op_alu(2 downto 0) <= "100" when op="000110" else op(5 downto 3);

r_op_alu(2 downto 0) <= opx(5 downto 3);
ri_op_alu(2 downto 0) <= opx(5 downto 3);


i_op_alu(5 downto 3) <= 
	"011" when op(2 downto 0)="000" else
	"000" when op(5 downto 3)="000" else
	"100";
	
iu_op_alu(5 downto 3) <=
 "011" when op(5)='1' else
   "10-";

br_op_alu(5 downto 3) <= "011" when op="000110" else "011";

with opx(2 downto 0) select r_op_alu(5 downto 3) <=
	"110" when "011",
	"100" when "110",
	"011" when "000",
	"00"&opx(3) when others;
	
ri_op_alu(5 downto 3) <= "110";

op_alu<=s_op_alu;

P: Process (clk,op,opx) is
begin
if(reset_n='0') then s_op_alu<="000000";
elsif(rising_edge(clk)) then 
if (op="111010") then
				-- R_type
				if opx(2 downto 0)="010" then
					s_op_alu <= ri_op_alu;
				elsif (opx(2 downto 0)="101" and opx(5 downto 4)="00") then
					s_op_alu <= "000000";
				elsif (opx="110100") then
					s_op_alu<= "000000";
				elsif opx="011101" then
					s_op_alu <= "000000";
				else
					s_op_alu<= r_op_alu;
				end if;
-- I_TYPE
elsif (op="010111") then
				s_op_alu<= "000000";
elsif (op="010101") then
				s_op_alu<= "000000";
elsif (op(3 downto 0)="0110" OR op(3 downto 0)="1110")  then
				s_op_alu<= br_op_alu;
elsif (op="000000") then
				s_op_alu <= "000000";
elsif (op="101000")or(op="110000") OR op(3 downto 0)="1100" OR op="010100"  then
				s_op_alu <= iu_op_alu;
elsif (op="000001") then
				s_op_alu <= "000000";
else
				s_op_alu<= i_op_alu;
end if;	
end if;

end Process;

--with State select op_alu <=
	--i_op_alu when I_OP,
	--iu_op_alu when IU_OP,
	--r_op_alu when R_OP,
	--ri_op_alu when RI_OP,
	--br_op_alu when BRANCH,
	--"000000" when others;	


	

update_state : process(clk,reset_n) is
begin
	if reset_n='0' then
		State <= FETCH_1;
	elsif (rising_edge(clk)) then
		case State is 
		when FETCH_1 => 
			State <= FETCH_2;
		when FETCH_2 =>
			State <= DECODE;
		when DECODE =>
			if (op="111010") then
				-- R_type
				if opx(2 downto 0)="010" then
					State <= RI_OP;
				elsif (opx(2 downto 0)="101" and opx(5 downto 4)="00") then
					State <= JMP;
				elsif (opx="110100") then
					State <= BREAK;
				elsif opx="011101" then
					State <= CALLR;
				else
					State <= R_OP;
				end if;
			-- I_TYPE
			elsif (op="010111") then
				State <= LOAD_1;
			elsif (op="010101") then
				State <= STORE;
			elsif (op(3 downto 0)="0110" OR op(3 downto 0)="1110")  then
				State <= BRANCH;
			elsif (op="000000") then
				State <= CALL;
			elsif (op="101000")or(op="110000") OR op(3 downto 0)="1100" OR op="010100"  then
				State <= IU_OP;
			elsif (op="000001") then
				State <= JMPI;
			else
				State <= I_OP;
			end if;	
		when BREAK =>
			State <= BREAK;
		when LOAD_1 =>
			State <= LOAD_2;
		when others =>
			State <= FETCH_1;
		end case;
	end if;
			
end process;

--read
with State select read <= 
'1' when FETCH_1,
'1' when LOAD_1, 
'0' when others;

--pc_en
with State select pc_en <=
'1' when FETCH_2,
'1' when CALL,
'1' when CALLR,
'1' when JMP,
'1' when JMPI,
'0' when others;

--ir_en
ir_en <= '1' when State = FETCH_2 else
'0';


--imm_signed
with State select imm_signed <= 
'1' when I_OP,
'1' when STORE,
'1' when LOAD_1,
'0' when others;

--rf_wren
with State select rf_wren <=
'1' when I_OP,
'1' when R_OP,
'1' when RI_OP,
'1' when IU_OP,
'1' when LOAD_2,
'1' when CALL,
'1' when CALLR,
'0' when others;

--sel_b
with State select sel_b <=
'1' when R_OP,
'1' when BRANCH,
'0' when others;

--sel_rC
sel_rC <= '1' when State = R_OP else
'1' when State = RI_OP else
--'1' when State=CALLR else
'0';



--sel_addr
sel_addr <= '1' when State = LOAD_1 else
'1' when State = STORE else
'0';

--sel_mem
sel_mem <= '1' when State = LOAD_2 else
'0';

--write
write <= '1' when State = STORE else
'0';

--branch_op
branch_op <= '1' when State = BRANCH  else
'0';

--pc_sel_imm
pc_sel_imm <= '1' when State = CALL else
'1' when State = JMPI else
'0';

--sel_pc
sel_pc <= '1' when State = CALL else
'1' when State= CALLR else 
'0';

--sel_ra
sel_ra <= '1' when State = CALL else
'0';

--pc_sel_a
pc_sel_a <= '1' when State = CALLR else
'1' when State = JMP else
--'1' when State = JMPI else
'0';

--pc_add_imm
pc_add_imm<='1' when State=BRANCH else '0';




end synth;
