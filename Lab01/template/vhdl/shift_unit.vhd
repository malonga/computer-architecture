library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity shift_unit is
    port(
        a  : in  std_logic_vector(31 downto 0);
        b  : in  std_logic_vector(4 downto 0);
        op : in  std_logic_vector(2 downto 0);
        r  : out std_logic_vector(31 downto 0)
    );
end shift_unit;

architecture synth of shift_unit is
	signal rot_left,rot_right,shll,shrl,shra : std_logic_vector(31 downto 0);
begin
with op select 
	r<= rot_left WHEN "000", --rotate left
	rot_right WHEN "001", -- rotate right
	shll WHEN "010", --shift left logical
	shrl WHEN "011", -- shift right logical
	shra WHEN "111", -- shift right arithmetic
	(31 downto 0 => '0') WHEN others;

compute_signal : Process(a,b) is
variable n : integer := to_integer(unsigned(b));
	begin
	if (n>0) then 
		shll(31 downto n)<=a((31-n) downto 0);
		shll<=shll & (n-1 downto 0 => '0') ;
		shrl((31-n) downto 0)<=a(31 downto n);
		shrl<=shrl & (31 downto (31-n + 1) => '0') ;
		shra(31 downto (31-n+1))<=(31 downto (31-n+1) => a(31));
		shra((31-n) downto 0)<=a(31 downto n);
		rot_left(n-1 downto 0)<=a(31 downto (31-n +1));
		rot_left(31 downto n)<=a(31-n downto 0);	
		rot_right(31 downto 31-n+1)<=a(n-1 downto 0);
		rot_right(31-n downto 0)<=a(31 downto n);	
	else
		shll<=(31 downto 0 => '0');
		shrl<=(31 downto 0 => '0');
		shra<=(31 downto 0 => '0');
		rot_left<=(31 downto 0 => '0');
		rot_right<=(31 downto 0 => '0');
	end if;

end process;
end synth;
