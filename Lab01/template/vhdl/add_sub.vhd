library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity add_sub is
    port(
        a        : in  std_logic_vector(31 downto 0);
        b        : in  std_logic_vector(31 downto 0);
        sub_mode : in  std_logic;
        carry    : out std_logic;
        zero     : out std_logic;
        r        : out std_logic_vector(31 downto 0)
    );
end add_sub;

architecture synth of add_sub is
	signal b_add : std_logic_vector(31 downto 0);
	signal sum : std_logic_vector(31 downto 0);
	signal sub : std_logic_vector(31 downto 0) := (others => sub_mode);
begin
	sub<=(31 downto 0 => sub_mode);
	b_add<=b xor sub;

	Addition : process(a,b_add,sub_mode)
		variable u:std_logic;
	begin
	u := sub_mode;
	for i in 0 to 31 loop
	sum(i) <= a(i) xor b_add(i) xor u;
	u := (a(i) and b_add(i)) or (a(i) and u) or (b_add(i) and u);
	end loop;
	carry<=u;
	end process Addition;

	update_zero : process(sum)
	begin
	if (to_integer(unsigned(sum))=0) then
		zero <='1'; 
	else
		zero<='0';
	end if;
	end process update_zero;

	r<=sum;
		

end synth;
