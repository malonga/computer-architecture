library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RAM is
    port(
        clk     : in  std_logic;
        cs      : in  std_logic;
        read    : in  std_logic;
        write   : in  std_logic;
        address : in  std_logic_vector(9 downto 0);
        wrdata  : in  std_logic_vector(31 downto 0);
        rddata  : out std_logic_vector(31 downto 0));
end RAM;

architecture synth of RAM is


type memory is array(0 to 1023) of std_logic_vector(31 downto 0);

signal memo_array: memory := (others=>(others=>'0'));
signal reset_n:std_logic;--???????????

begin

writing: process(clk,cs,write) is
begin

	if(rising_edge(clk)) then 
		if(cs='1' AND write='1') then 
			memo_array(to_integer(unsigned(address)))<=wrdata; 
		end if;
	end if;
	
end process writing;


reading: process(clk,cs,read) is
begin

	if(rising_edge(clk)) then 
		if(cs='1' AND read='1') then
			rddata<=memo_array(to_integer(unsigned(address)));
		else rddata<=(31 downto 0 => 'Z');
		end if; 
	end if;
			
end process reading;

end synth;
