library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_register_file is
end;

architecture bench of tb_register_file is

    -- declaration of register_file interface
    -- INSERT COMPONENT DECLARATION HERE
    component register_file is 
		port(
			clk    : in  std_logic;
			aa     : in  std_logic_vector(4 downto 0);
			ab     : in  std_logic_vector(4 downto 0);
			aw     : in  std_logic_vector(4 downto 0);
			wren   : in  std_logic;
			wrdata : in  std_logic_vector(31 downto 0);
			a      : out std_logic_vector(31 downto 0);
			b      : out std_logic_vector(31 downto 0)
		);
    end component;

    signal aa, ab, aw   : std_logic_vector(4 downto 0);
    signal a, b, wrdata : std_logic_vector(31 downto 0);
    signal wren         : std_logic := '0';
    -- clk initialization
    signal clk, stop    : std_logic := '0';
    -- clk period definition
    constant CLK_PERIOD : time      := 40 ns;

begin

    -- register_file instance
    -- INSERT REGISTER FILE INSTANCE HERE
    register_file_0 : register_file port map(
		clk => clk,
		aa => aa,
		ab => ab,
		aw => aw,
		wren => wren,
		wrdata => wrdata,
		a => a,
		b => b
    );

    clock_gen : process
    begin
        -- it only works if clk has been initialized
        if stop = '0' then
            clk <= not clk;
            wait for (CLK_PERIOD / 2);
        else
            wait;
        end if;
    end process;

    process
    begin
        -- init
        wren   <= '0';
        aa     <= "00000";
        ab     <= "00001";
        aw     <= "00000";
        wrdata <= (others => '0');
        wait for 5 ns;

        -- write in the register file
        wren <= '1';
        for i in 0 to 31 loop
            -- std_logic_vector(to_unsigned(number, bitwidth))
            aw     <= std_logic_vector(to_unsigned(i, 5));
            wrdata <= std_logic_vector(to_unsigned(i + 1, 32));
            wait for CLK_PERIOD;
        end loop;

        -- read in the register file
        -- INSERT CODE THAT READS THE REGISTER FILE HERE
	-- aa checks even address and ab checks uneven addresses
	-- such that ab = aa + 1 at beginning, end and during most of the loop
        -- special case addr 0 should be 0

	aa <= std_logic_vector(to_unsigned(0,5));
	ab <= std_logic_vector(to_unsigned(1,5));
	wait for 20 ns; -- wait for circuit to react
	assert a = std_logic_vector(to_unsigned(0, 32))
		report "Test 1a : Incorrect read after write on a"
		severity warning;
	assert b = std_logic_vector(to_unsigned(2, 32))
		report "Test 1a : Incorrect read after write on b"
		severity warning;
	

	for i in 1 to 15 loop
			aa <= std_logic_vector(to_unsigned(2*i,5));
			ab <= std_logic_vector(to_unsigned(2*i+1,5));
			wait for 20 ns; -- wait for circuit to react
			assert a = std_logic_vector(to_unsigned(2*i + 1, 32))
				report "Test 1 : Incorrect read after write on a"
				severity warning;
			assert b = std_logic_vector(to_unsigned(2*i + 2, 32))
				report "Test 1 : Incorrect read after write on b"
				severity warning;
	end loop;
	-- Same except this time aa = ab + 1 | in order to mitigate the possibility
	-- of a dependence relationship between both 
	ab <= std_logic_vector(to_unsigned(0,5));
	aa <= std_logic_vector(to_unsigned(1,5));
	wait for 20 ns; -- wait for circuit to react
	assert b = std_logic_vector(to_unsigned(0, 32))
		report "Test 2a : Incorrect read after write on b"
		severity warning;
	assert a = std_logic_vector(to_unsigned(2, 32))
		report "Test 2a : Incorrect read after write on a"
		severity warning;

	for i in 1 to 15 loop
		ab <= std_logic_vector(to_unsigned(2*i,5));
		aa <= std_logic_vector(to_unsigned(2*i+1,5));
		wait for 20 ns; -- wait for circuit to react
		assert b = std_logic_vector(to_unsigned(2*i + 1, 32))
			report "Test 2b : Incorrect read after write on b"
			severity warning;
		assert a = std_logic_vector(to_unsigned(2*i + 2, 32))
			report "Test 2b : Incorrect read after write on a"
			severity warning;
	end loop;
		
        stop <= '1';
        wait;
    end process;
end bench;
